const { resolve, join } = require('path');
const webpack           = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require ('copy-webpack-plugin');
const { logInfo }  		= require('./building_helpers/debugOutup');
const log 				= require('loglevel');

const { mergeProps, loadEnviFile } 	= require('./building_helpers/envifyBuild');

module.exports = (function(){
	let env       = process.env;
	let basePath  = resolve(__dirname, './');
	let context   = join(basePath, env.npm_package_config_src_dir);
	let output    = env.npm_package_config_public_dir;

	logInfo('> SRC DIR: ' 	 + context);
	logInfo('> OUTPUT DIR: ' + output);
	
	return mergeProps({

		context : context,

		entry   : { 
			main: './js/main.jsx'
		},

		output: {
			path      : join(basePath, output),
			filename  : 'js/[name].bundle.js'
		},

		resolve: {
			extensions: ['.js', '.jsx']
		},

		module: {
			loaders: [{
				test    : /\.(js|jsx)$/,
				loaders : ['babel-loader'],
				exclude : [/node_modules/, /test/]
			}, {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                	publicPath: '../',
                	fallbackLoader: "style-loader",
                	loader: "css-loader?sourceMap"
				})
            }, {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                	publicPath: '../',
                	fallbackLoader: "style-loader",
                	loader: "css-loader?sourceMap!sass-loader?sourceMap"
				})
            }, {
                test: /\.(svg|otf|ttf|woff|woff2|eot)(\?v=\d+\.\d+\.\d+)?$/,
                loader: ['url-loader']
            }, {
                test: /\.(gif|png|jpg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: ['file-loader?name=assets/images/[name].[ext]']
            }, {
                test: /\.(.wav|mp3)(\?v=\d+\.\d+\.\d+)?$/,
                loader: ['file-loader?name=assets/audio/[name].[ext]&limit=10000&mimetype=audio/wav']
            }]
		},

		plugins: [
			new webpack.EnvironmentPlugin(['NODE_ENV']),
			new webpack.DefinePlugin({
				VERSION: JSON.stringify("5fa3b9"),
			}),
			new HtmlWebpackPlugin({
				title: env.npm_package_name,
				template : './index.html'
			}),
			new ExtractTextPlugin("css/[name].css"),
			new CopyWebpackPlugin([{
				context: './json',
				from: '*',
				to: join(basePath, output, 'json')
			}])
		],

		stats: {
	        colors : true
	    },

		devtool : 'source-map'
		
	}, loadEnviFile(basePath));
})();