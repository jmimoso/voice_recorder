import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { detect } from 'detect-browser';
import saveAs from 'save-as';

import LoaderComponent from '../components/LoaderComponent';
import MessageComponent from '../components/MessageComponent';
import EntryController from './EntryController';

import MediaStreamRecorder from 'msr';

const browser = detect();
const constraints = window.constraints = {
	audio: true,
	video: false
};


export default class AppController extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			compatible: this.checkForCompatibility()
		};
	}

	componentWillMount() {
		if(this.state.compatible) {
			this.checkForMic();
		} else {
			this.setState({
				loading: false
			});
		}
	}

	render() {
		return (
			<section className='main-content'>
				{this._renderComponent()}
			</section>
		);
	}

	_renderComponent() {
		if(this.state.loading) {
			return this._renderLoader();
		} else {
			if(this.state.compatible) {
				if(this.state.hasMic) {
					return this._renderContent();
				} else {
					return this._renderMessage('mic');
				}
			} else {
				return this._renderMessage('compatibility');
			}
		}
	}

	_renderContent() {
		return (
			<Router>
				<Switch>
					<Route exact path='/' render={() => <EntryController startRecording={this.startRecording} stopRecording={this.stopRecording} />} />
				</Switch>
			</Router>
		);
	}

	_renderLoader() {
		return (
			<LoaderComponent />
		);
	}

	_renderMessage(type) {
		return (
			<MessageComponent type={type} />
		);
	}

	checkForCompatibility() {
		if(window.AudioContext || window.webkitAudioContext) {
			return true;
		} else {
			return false;
		}
		
	}

	checkForMic = () => {
		navigator.mediaDevices.getUserMedia(constraints)
		.then(stream => {
			console.log('1111111111111')
			this.initAudioRecorder(stream);
			this.setState({
				hasMic: true,
				loading: false
			});
		})
		.catch(err => {
			console.log('22222222222222')
			this.setState({
				hasMic: false,
				loading: false
			});
		});
	}

	initAudioRecorder(stream) {
		this.mediaRecorder = new MediaStreamRecorder(stream);
		this.mediaRecorder.mimeType = 'audio/wav';
		this.mediaRecorder.ondataavailable = (blob) => this.saveBlob(blob);
	}

	startRecording = () => {
		this.mediaRecorder.start(this.setRecordingInterval());
	}

	stopRecording = () => {
		this.mediaRecorder.stop();
		saveAs(this.audioBlob, 'HighFiveAudio.wav');
		
	}

	saveBlob = (blob) => {
		this.audioBlob = blob;
	}

	setRecordingInterval() {
		return 36000000;
	}


}