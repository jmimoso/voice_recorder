import React, { Component } from 'react';
import { TweenLite } from 'gsap';
import ReactSVG from 'react-svg';


export default class EntryController extends Component {

	constructor(props) {
		super(props);
		this.state = {
			recording: false
		};
	}

	render() {
		return (
			<section className='entry-wrapper bg'>
				<div className='entry-container'>
					<div className='entry-content'>
						<div className='title-wrapper'>
							<div className='title-container'>
								<ReactSVG 
									path='./assets/images/logo.svg'
								    wrapperClassName='title-image-wrapper'
								    className='title-image'>
								</ReactSVG>
							</div>
						</div>
						<div className='button-wrapper'>
							<div className='button-container'>
								<div className='button-image-wrapper'>
									<div className='button-content' onClick={this.onRecord}>
										<ReactSVG
											path={this.state.recording ? './assets/images/mic_on.svg' : './assets/images/mic.svg'}
										    className='button-image'
										    wrapperClassName='button-image-container'>
										</ReactSVG>
									</div>
								</div>
								<ReactSVG
									path='./assets/images/step_one.svg'
								    className='stepOne-image'
								    wrapperClassName='stepOne-image-container'>
								</ReactSVG>
								<ReactSVG
									path='./assets/images/step_two.svg'
								    className='stepTwo-image'
								    wrapperClassName='stepTwo-image-container'>
								</ReactSVG>
							</div>
						</div>


					</div>
				</div>
			</section>
		);
	}

	onRecord = (ev) => {
		this.setState({
			recording: !this.state.recording
		}, () => {
			if(this.state.recording) {
				this.props.startRecording();
			} else {
				this.props.stopRecording();
			}
		});
	}


}