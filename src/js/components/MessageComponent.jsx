import React, { Component } from 'react';

import { FaExclamationTriangle, FaMicrophoneSlash } from 'react-icons/lib/fa/';


export default class MessageComponent extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {
		return (
			<div className='message-wrapper'>
				{this.setMessage()}
			</div>
		);
	}

	setMessage() {
		if(this.props.type === 'compatibility') {
			return (
				<div className='message-container'>
					<div className='message-icon-wrapper'>
						<FaExclamationTriangle className='message-icon' size={36} />
					</div>
					<div className='message-text-wrapper'>
						<p>Your browser does not support this feature!</p>
						<p>Try <a href='https://www.google.com/chrome/' target='_blank'>Google Chrome</a>.</p>
					</div>
				</div>
			);
		} else {
			return (
				<div className='message-container'>
					<div className='message-icon-wrapper'>
						<FaMicrophoneSlash className='message-icon' size={36} />
					</div>
					<div className='message-text-wrapper'>
						<p>Please connect or activate your microphone!</p>
					</div>
				</div>
			);
		}
	}


}